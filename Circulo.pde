class Circulo{
   float h;
   float k;
   float radio;
   Circulo(float a, float b,float c){
   h = a;
   k = b;
   radio = c;
   }
   
   void dibujaCirculo(){
      stroke(0);
      noFill();
      ellipse(h,k,2*radio,2*radio);
   }
   
   boolean isAdentro(float a, float b){
      return dist(h,k,a,b) <= radio;
   }
}