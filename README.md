El proyecto cuenta con las siguientes clases.
1 - Punto.pde
2 - Circulo.pde
3 - Vector.pde

El sketch se encarga de crear los objetos y presentar el objeto vector para interactuar con el.
Puede usar el apuntador del mouse para colocarlo cerca de los puntos destino u origen y manteniendo presionado el boton del mouse, puede mover los puntos origen y destino en donde desee.

Si gusta acceder al vídeo donde se explica cómo se llevo a cabo el desarrollo, puede hacerlo a través del siguiente enlace:
https://www.youtube.com/watch?v=G91qcRUDa0k&t=24s 