Vector v1;
Punto p1;
Punto p2;
float dxo, dyo, dyd, dxd, dxov, dyov,dxdv, dydv;
String msg;
boolean bano, band, banv;
void setup(){
   size(400,400);
   p1 = new Punto(40, height-70);
   p2 = new Punto(width-40, 70);
   v1 = new Vector(p1,p2);
   dxo=dyo=dxd=dyd=dxov=dyov=dxdv=dydv=0;
   bano = band = banv = false;
   msg="AQUI VA ALGO";
}

void draw(){
    background(255);
    v1.dibujaVector();
    fill(0);
    text(msg,40,40);
}

void mousePressed(){
   if(v1.co.isAdentro(mouseX, mouseY)){
      dxo = -(v1.co.h - mouseX);
      dyo = -(v1.co.k - mouseY);
      msg = "ADENTRO => " + "("+dxo+ " , "+dyo+")";
      bano = true;
   }
   if(v1.cd.isAdentro(mouseX, mouseY)){
      dxd = -(v1.cd.h - mouseX);
      dyd = -(v1.cd.k - mouseY);
      msg = "ADENTRO => " + "("+dxd+ " , "+dyd+")";
      band = true;
   }
   
   if(v1.isVector(mouseX, mouseY)){
        msg = "tocando vector";
        dxdv = -(v1.cd.h - mouseX);
        dydv = -(v1.cd.k - mouseY);
        dxov = -(v1.co.h - mouseX);
        dyov = -(v1.co.k - mouseY);
        banv = true;
   }
}

void mouseReleased(){
   bano = band = banv = false;
}

void mouseDragged(){
    if(bano){
       v1.co.h = mouseX - dxo;
       v1.co.k = mouseY - dyo;
    }
    if(band){
       v1.cd.h = mouseX - dxd;
       v1.cd.k = mouseY - dyd;    
    }
    
    if(banv){
        v1.cd.h = mouseX - dxdv;
        v1.cd.k = mouseY - dydv;
        v1.co.h = mouseX - dxov;
        v1.co.k = mouseY - dyov;
    }
    
}