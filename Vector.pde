class Vector{
    Punto origen;
    Punto destino;
    Circulo co;
    Circulo cd;
    float norma;
    Vector(Punto a, Punto b){
       origen = a;
       destino = b;
       co = new Circulo(origen.x, origen.y, 5);
       cd = new Circulo(destino.x, destino.y, 5);
       norma = dist(co.h, co.k, cd.h, cd.k);
    
    }
    
    void dibujaVector(){
       stroke(0);
       strokeWeight(2);
       line(co.h,co.k, cd.h, cd.k);
       
       pushMatrix();
           translate(cd.h, cd.k);
           float a = atan2(co.h-cd.h, cd.k-co.k);
           rotate(a);
           line(0,0,-10,-10);
           line(0,0,10,-10);
           triangle(-10,-10,10,-10,0,0);
       popMatrix();
    }
    
    boolean isVector(float a, float c){
      if (cd.h > co.h){      
         if (a > co.h+7 && a < cd.h-7) {
             //print("en zona x");
             float m = (cd.k-co.k)/ (cd.h-co.h);
             float b = co.k - m*co.h;
             float ytmp = m*a + b;
             //ellipse(a, ytmp,20,20);
             print("c= "+c+"    ytmp="+ytmp+"\n");
             if (c > ytmp-3 && c < ytmp+3 ){
                print("tocando vector");
                return true;
             }
         }
      }// comprobacion de cd.h > co.h

     if (co.h > cd.h){      
         if (a > cd.h+7 && a < co.h-7) {
             //print("en zona x");
             float m = (cd.k-co.k)/ (cd.h-co.h);
             float b = co.k - m*co.h;
             float ytmp = m*a + b;
             print("c= "+c+"    ytmp="+ytmp+"\n");
             if (c > ytmp-3 && c < ytmp+3 ){
                print("tocando vector");
                return true;
             }
         }
      }// comprobacion de cd.h > co.h
    
         return false;
    }
    
    
    
}